terraform {
  required_version = ">= 0.15"

  required_providers {
    aws  = ">= 3.45.0"
    null = "~> 2.1"
  }
}
